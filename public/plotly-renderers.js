(function () {
  var t;
  (t = function (t) {
    return "object" == typeof exports && "object" == typeof module
      ? t(require("jquery"), require("plotly.js"))
      : "function" == typeof define && define.amd
      ? define(["jquery", "plotly.js"], t)
      : t(jQuery, Plotly);
  })(function (t, e) {
    var r, o;
    return (
      (r = function (r, o, n) {
        return (
          null == r && (r = {}),
          null == o && (o = {}),
          null == n && (n = !1),
          function (i, l) {
            var a, s, u, h, d, p, y, g, c, v, j, f;
            return (
              (h = {
                localeStrings: {
                  vs: "vs",
                  by: "by",
                },
                plotly: {},
              }),
              (l = t.extend(!0, {}, h, l)),
              (v = i.getRowKeys()),
              (a = i.getColKeys()),
              (f = n ? a : v),
              0 === f.length && f.push([]),
              (u = n ? v : a),
              0 === u.length && u.push([]),
              (d = i.aggregatorName),
              i.valAttrs.length && (d += "(" + i.valAttrs.join(", ") + ")"),
              (s = f.map(function (e) {
                var o, l, a, s, h, p, y;
                for (y = [], a = [], l = 0, s = u.length; l < s; l++)
                  (o = u[l]),
                    (p = parseFloat(
                      i.getAggregator(n ? o : e, n ? e : o).value()
                    )),
                    y.push(isFinite(p) ? p : null),
                    a.push(o.join("-") || " ");
                return (
                  (h = {
                    name: e.join("-") || d,
                  }),
                  (h.x = n ? y : a),
                  (h.y = n ? a : y),
                  t.extend(h, r)
                );
              })),
              n
                ? ((y = i.rowAttrs.join("-")), (p = i.colAttrs.join("-")))
                : ((y = i.colAttrs.join("-")), (p = i.rowAttrs.join("-"))),
              (j = d),
              "" !== y && (j += " " + l.localeStrings.vs + " " + y),
              "" !== p && (j += " " + l.localeStrings.by + " " + p),
              (g = {
                title: j,
                hovermode: "closest",
                width: window.innerWidth / 1.4,
                height: window.innerHeight / 1.4 - 50,
                xaxis: {
                  title: n ? d : null,
                  automargin: !0,
                },
                yaxis: {
                  title: n ? null : d,
                  automargin: !0,
                },
              }),
              (c = t("<div>").appendTo(t("body"))),
              e.newPlot(c[0], s, t.extend(g, o, l.plotly)),
              c.detach()
            );
          }
        );
      }),
      (o = function () {
        return function (r, o) {
          var n, i, l, a, s, u, h, d, p, y, g, c, v, j;
          for (
            a = {
              localeStrings: {
                vs: "vs",
                by: "by",
              },
              plotly: {},
            },
              o = t.extend(!0, {}, a, o),
              v = r.getRowKeys(),
              0 === v.length && v.push([]),
              i = r.getColKeys(),
              0 === i.length && i.push([]),
              l = {
                x: [],
                y: [],
                text: [],
                type: "scatter",
                mode: "markers",
              },
              s = 0,
              d = v.length;
            s < d;
            s++
          )
            for (c = v[s], u = 0, p = i.length; u < p; u++)
              (n = i[u]),
                (j = r.getAggregator(c, n).value()),
                null != j &&
                  (l.x.push(n.join("-")),
                  l.y.push(c.join("-")),
                  l.text.push(j));
          return (
            (h = {
              title: r.rowAttrs.join("-") + " vs " + r.colAttrs.join("-"),
              hovermode: "closest",
              xaxis: {
                title: r.colAttrs.join("-"),
                domain: [0.1, 1],
              },
              yaxis: {
                title: r.rowAttrs.join("-"),
              },
              width: window.innerWidth / 1.5,
              height: window.innerHeight / 1.4 - 50,
            }),
            (y = t("<div>", {
              style: "display:none;",
            }).appendTo(t("body"))),
            (g = t("<div>").appendTo(y)),
            e.plot(g[0], [l], t.extend(h, o.plotly)),
            g.detach(),
            y.remove(),
            g
          );
        };
      }),
      (t.pivotUtilities.plotly_renderers = {
        堆叠图: r(
          {
            type: "bar",
          },
          {
            barmode: "relative",
          }
        ),
        // "Line Chart": r(),
        // "Area Chart": r({
        //   stackgroup: 1
        // }),
        // 热力图和堆叠图: r(
        //   { type: "bar" },
        //   {
        //     barmode: "relative",
        //   }
        // ),
      })
    );
  });
}).call(this);
//# sourceMappingURL=plotly_renderers.min.js.map
